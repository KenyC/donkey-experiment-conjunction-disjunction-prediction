---
title: Verification of the interpretation of conjunction under the scope of an existential
authors: Keny Chatain, Benjamin Spector
---

# Introduction


The goal is to test the possible readings of sentences containing pronouns. We specifically study two types of sentences: conjunctive sentences on the model of (CONJ) and disjunctive sentences on the model of  (DISJ). 

:::{.ex id=model}
a. There is a circle and it is blue.
b. Either there isn't a circle or it is blue.
:::

3 potential readings of such sentences have been discussed in the linguistic literature:

 1. The *universal reading* where "it" means "all of the triangles"
 2. The *existential reading* where "it" means "some of the triangles"
 3. The *uniquenesss reading* where "it" means "the unique triangle"

We test which of these readings is available in a picture verification task.


Participants are asked to rate how true a certain predictions feels as a description of the picture provided, on a scale from 1 to 7. For each sentence type, we construct 5 types of picture which makes 0, 1, 2 or 3 of the potential readings of interest true, plus any additional controls (cf below)


# Design

Participants complete a series of 30 trials. In each trial, a sentence and a picture are presented. In instructions presented at the beginning of the experiment, it is explained that the sentence represents the prediction made by a character (Mary) about the picture and that Mary has not seen the picture. Participants are asked to rate "*how true the prediction feels to \[them]*" on a scale ranging from 1 (*completely false*) to 7 (*completely true*). The full instructions are given below:

> This task includes several trials.  
   In each trial, you will see a prediction that Mary made about a picture that she hasn't seen. The picture, which you will see, contains several geometrical shapes. You will have to rate how true Mary's prediction feels to you given the picture.  
   There is no "correct answer". What we are interested in is your intuitive judgment

We chose to ask participants to check a prediction made by a character, instead of asking directly how true they judge the sentences of interest, because we worried that participants may judge the disjunctive sentences to be tested inappropriate in a task where all information is available. It is known that disjunction give rise to ignorance inferences. By explicitly setting a context where ignorance is assumed, we give reason why a disjunctive sentence might have been used.

Each trial belongs to a condition which is a pair of a sentence type with a picture type. Each condition has 3 trials. We start by describing the picture type used and the sentence type. Below is an image showing a typical trial (CONJ-UNIQUE condition):

![Example of a NEG-CONJ1FALSE trial](example_trial.png)

## Sentence types

 1. **CONJ:** There will be a SHAPE and it will be COLOR.
 2. **DISJ:** Either there won't be a SHAPE or it will be COLOR.

Because the task is about predictions, sentences are presented with future tense.

Where:

 - SHAPE, SHAPE1, SHAPE2 are either *circle*, *triangle* or *square*
 - COLOR is either *red*, *green* or *blue*

## Picture types

Each picture contains 4 colored shapes displayed a square configuration (cf picture above). We describe the picture types for the case SHAPE = triangle, COLOR = blue
 
 - **CONJ-CONJ1FALSE:** there is no triangle
 - **CONJ-CONJ2FALSE:** there is only one triangle ; that triangle is not blue
 - **CONJ-EXISTS:** there is one blue triangle, there is one non-blue triangle, other shapes are not triangles
 - **CONJ-ALL:** there are two blue triangles, other shapes are not triangles
 - **CONJ-UNIQUE:** there is one blue triangle, other shapes are not triangles


 - **DISJ-FALSE:** there is one non-blue triangle, other shapes are not triangles
 - **DISJ-EXISTS:** there is one blue triangle, there is one non-blue triangle, other shapes are not triangles
 - **DISJ-ALL:** there are two non-blue triangles, other shapes are not triangle
 - **DISJ-UNIQUE:** there is one blue triangle, other shapes are not triangles
 - **DISJ-TRUE1:** there is no triangle




## Conditions

Conditions are named after picture types and they are associated with the corresponding sentence type. For instance, a trial of the CONJ-CONJ1FALSE condition consists of a picture of the CONJ-CONJ1FALSE picture type associated with a sentence of the CONJ type.


# Hypothesis

  - **H-CONJ-EXIST:** The existential reading exists for conjunction type sentences    
    *Operationalized as:* CONJ sentences paired with CONJ-EXISTS pictures are rated on average higher than when paired with CONJ-CONJ2FALSE
    
  - **H-CONJ-ALL:** The universal reading exists for conjunction type sentences  
    *Operationalized as:* CONJ sentences paired with CONJ-ALL is rated on average higher than when paired with CONJ-EXISTS
    
  - **H-CONJ-UNIQUE:** The uniqueness reading exists for conjunction type sentences  
    *Operationalized as:* CONJ sentences paired with CONJ-UNIQUE is rated on average higher than when paired with CONJ-ALL
    
  - **H-DISJ-EXISTs:** The existential reading exists for disjunction type sentences  
    *Operationalized as:* DISJ sentences paired with DISJ-EXISTENTS is rated on average higher than when paired with DISJ-FALSE
    
  - **H-DISJ-ALL:** The universal reading exists for disjunction type sentences  
    *Operationalized as:* DISJ paired with DISJ-ALL is rated on average higher than when paired with DISJ-EXISTS
    
  - **H-DISJ-UNIQUE:** The uniqueness reading exists for disjunction type sentences  
    *Operationalized as:* DISJ paired with DISJ-UNIQUE is rated on average higher than when paired with DISJ-ALL
    
  - **H-CONJ-DISJ:** A universal reading is more easily accessed in disjunction type sentences than in conjunction-type sentences   
    *Operationalized as:* The average difference in ratings between the CONJ-EXISTS and CONJ-ALL is smaller than the average difference in ratings between DISJ-EXISTS and DISJ-ALL
 


# Sampling plan

## Data collection procedures

We will recruit participants through the Prolific platform, selecting only native speakers of English who are not colorblind. Each participant will be paid £2.25


## Sample size

We plan to recruit 120 participants based on a power analysis conducted using the results from two previous experiments, adressing similar questions: <https://osf.io/n4vft> and <https://osf.io/8jzs4>


# Analysis plan

## Statistical models

For all hypotheses listed in the [[#Hypothesis]] but H-CONJ-DISJ, we do:

 - subset our data to the two conditions that will be compared
 - fit a cumulative link model with logit link that includes slopes for the condition factor, and a random intercept per participant as a random effect
 - compare this model to one that does not include a slope for the condition factor
 - compare the two models using the *anova* function

For H-CONJ-DISJ, we do:

 - subset our data to the conditions CONJ-EXISTS, CONJ-ALL, DISJ-EXISTS, DISJ-ALL
 - fit a cumulative link model with logit link that includes one slopes for the group factor (CONJ vs DISJ), one slope for the picture-type (EXISTS vs ALL), an interaction term for these two factors, and a random intercept per participant as a random effect
 - compare this model to one that does not include the interaction factor.
 - compare the two models using the *anova* function

We will consider the difference between conditions to be statistically significant if the p-value returned by the model comparison (likelihood ration test) is below 0.05, correcting for multiple comparisons. Since we run 6 comparisons (one for each hypothesis), we apply the Holm-Bonferroni method to correct all p-values.

  
Cf attached R script for details.

## Data exclusion

 - We will exclude participants who report (in the final questionnaire) that they are colorblind, or that they are not native speakers of English.
 - We will exclude any participant who completed the experiment more than once or for whom only partial responses were collected
 - We will exclude participants who, at least twice (out of six trials), give normatively incorrect answers to CONTROL-BOTHCONJFALSE and CONTROL-TRUE conditions, where a normatively incorrect answer is:
    * for CONTROL-TRUE condition: any score of 4 or less
    * for CONTROL-BOTHCONJFALSE condition: any score of 3 or more